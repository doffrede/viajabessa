package com.bemobi.viajabessa;

import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bemobi.viajabessa.adapter.TravelAdapter;
import com.bemobi.viajabessa.model.Travel;
import com.bemobi.viajabessa.service.TravelService;

import java.util.List;

public class HomeActivity extends AppCompatActivity implements TravelService.TravelServiceInterface {

    private TravelAdapter adapter;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setTitle(getString(R.string.app_name));

        RecyclerView recycler = (RecyclerView) findViewById(R.id.recycler);
        recycler.setLayoutManager(new LinearLayoutManager(this));

        adapter = new TravelAdapter(this);
        recycler.setAdapter(adapter);

        showLoading();
        TravelService.getInstance(this).getTravel();
    }

    private void showLoading() {
        View v = getLayoutInflater().inflate(R.layout.progress_dialog, null);

        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.show();
        progressDialog.setContentView(v);
    }

    public void dismissLoadingDialog() {
        if (progressDialog == null) {
            return;
        }
        progressDialog.dismiss();


    }

    @Override
    public void handleError(VolleyError error) {
        dismissLoadingDialog();
    }

    @Override
    public void getTravelReturn(List<Travel> travels) {
        dismissLoadingDialog();
        adapter.setTravels(travels);
    }
}
