package com.bemobi.viajabessa;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.bemobi.viajabessa.adapter.ImagePageAdapter;
import com.bemobi.viajabessa.model.Travel;
import com.google.gson.Gson;
import com.kassisdion.library.ViewPagerWithIndicator;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

public class TravelActivity extends AppCompatActivity {

    private Travel travel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travel);

        Gson gson = new Gson();
        if (getIntent() != null) {
            travel = gson.fromJson(getIntent().getStringExtra("Travel"),
                    Travel.class);
        }

        createToolbar();
        populateTravel();

    }

    private void createToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        setTitle(travel.getName());
    }

    private void populateTravel() {

        configureViewPager();

        TextView name = (TextView) findViewById(R.id.name);
        TextView description = (TextView) findViewById(R.id.description);
        TextView price = (TextView) findViewById(R.id.price);
        TextView listPrice = (TextView) findViewById(R.id.list_price);

//        NumberFormat defaultFormat = NumberFormat.getCurrencyInstance(getCurrentLocale(this));
//        defaultFormat.format(num)

        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        format.setCurrency(Currency.getInstance("BRL"));

        name.setText(travel.getName());
        description.setText(travel.getDescription());
        price.setText(format.format(travel.getPrice()));
        listPrice.setText(format.format(travel.getListPrice()));
        listPrice.setPaintFlags(listPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        if (travel.getPrice() >= travel.getListPrice()) {
            listPrice.setVisibility(View.GONE);
        }
    }

    private void configureViewPager() {
        ViewPagerWithIndicator viewPagerIndicator = (ViewPagerWithIndicator) findViewById(R.id.view_pager_indicator);
        ViewPager imageViewPager = (ViewPager) findViewById(R.id.travel_image_slider);
        ImagePageAdapter adapter = new ImagePageAdapter(this, travel.getImages());

        imageViewPager.setAdapter(adapter);
        if (travel.getImages() != null && travel.getImages().size() > 1) {
            viewPagerIndicator.setViewPager(imageViewPager);
        } else {
            viewPagerIndicator.setVisibility(View.GONE);
        }

        ViewGroup.LayoutParams params = imageViewPager.getLayoutParams();
        params.height = getImageSize();
        imageViewPager.setLayoutParams(params);
    }

    private int getImageSize() {
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int windowWidth = size.x;

        return (windowWidth * 600) / 800;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);

    }

}
