package com.bemobi.viajabessa.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by davidoffrede on 22/08/17.
 */

public class ResizableImageView  extends android.support.v7.widget.AppCompatImageView {
    Context context;

    int imageHeight = 600;
    int imageWidth = 800;

    public ResizableImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public ResizableImageView(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        float imageRatio = 0.0F;
        if (imageHeight > 0) {
            imageRatio = imageWidth / imageHeight;
        }
        float sizeRatio = 0.0F;
        if (heightSize > 0) {
            sizeRatio = widthSize / heightSize;
        }

        int width;
        int height;
        if (imageRatio >= sizeRatio) {
            width = widthSize;
            height = width * imageHeight / imageWidth;
        } else {
            height = heightSize;
            width = height * imageWidth / imageHeight;
        }

        setMeasuredDimension(width, height);
    }
}
