package com.bemobi.viajabessa.service;

import android.os.Build;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bemobi.viajabessa.ViajabessaApplication;
import com.bemobi.viajabessa.model.Travel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by davidoffrede on 22/08/17.
 */

public class TravelService {

    public interface TravelServiceInterface {

        void handleError(VolleyError error);

        void getTravelReturn(List<Travel> travels);

    }

    private static TravelService instance;

    private TravelServiceInterface travelServiceInterface;

    private static String TAG = "TravelService";
    private static String tag_request = "json_obj_req";

    private TravelService(TravelServiceInterface travelServiceInterface) {
        this.travelServiceInterface = travelServiceInterface;
    }

    public static TravelService getInstance(TravelServiceInterface travelServiceInterface) {
        if (instance == null) {
            return new TravelService(travelServiceInterface);
        }

        instance.travelServiceInterface = travelServiceInterface;
        return instance;
    }

    public void getTravel() {
        String url = "http://private-3879c-doffrede.apiary-mock.com/travels";

        StringRequest req = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String str = "";
                        try {
                            str = new String(response.getBytes("ISO-8859-1"), "UTF-8");
                        } catch (UnsupportedEncodingException e) {

                            e.printStackTrace();
                        }

                        Log.d(TAG, str);

                        Gson gson = new Gson();

                        Type listType = new TypeToken<List<Travel>>() {
                        }.getType();
                        List<Travel> travels = gson.fromJson(str, listType);

                        travelServiceInterface.getTravelReturn(travels);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                travelServiceInterface.handleError(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Android-Version", Build.VERSION.RELEASE);
                params.put("Device-Brand", Build.BRAND);
                params.put("Device-Model", Build.MODEL);

                return params;
            }
        };

        ViajabessaApplication.getInstance().addToRequestQueue(req, tag_request);
    }

}
