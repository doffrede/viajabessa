package com.bemobi.viajabessa.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bemobi.viajabessa.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by davidoffrede on 22/08/17.
 */

public class ImagePageAdapter extends PagerAdapter {

    private List<String> images;
    private Context context;
    private LayoutInflater inflater;

    public ImagePageAdapter(Context context, List<String> images) {
        this.images = images;
        this.context = context;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return (images != null) ? images.size() : 1;
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View view = inflater.inflate(R.layout.adapter_image_page, container, false);

        ImageView image = (ImageView) view.findViewById(R.id.image);

        if (images != null && !TextUtils.isEmpty(images.get(position))) {
            Picasso.with(context)
                    .load(images.get(position))
                    .placeholder(R.drawable.placeholder)
                    .fit()
                    .centerInside()
                    .into(image);
        } else {
            image.setImageResource(R.drawable.placeholder);
        }

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
