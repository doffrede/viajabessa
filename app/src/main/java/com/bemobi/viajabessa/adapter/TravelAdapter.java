package com.bemobi.viajabessa.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bemobi.viajabessa.R;
import com.bemobi.viajabessa.TravelActivity;
import com.bemobi.viajabessa.model.Travel;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

/**
 * Created by davidoffrede on 22/08/17.
 */

public class TravelAdapter extends RecyclerView.Adapter<TravelAdapter.Holder> {

    private Context context;
    private List<Travel> travels;

    public TravelAdapter(Context context) {
        this.context = context;
        this.travels = new ArrayList<>();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private ImageView image;
        private TextView name, price;

        public Holder(View view) {
            super(view);

            image = (ImageView) view.findViewById(R.id.image);

            name = (TextView) view.findViewById(R.id.name);
            price = (TextView) view.findViewById(R.id.price);
        }
    }

    @Override
    public TravelAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_travel, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final TravelAdapter.Holder holder, int position) {

        final Travel travel = travels.get(position);

        holder.name.setText(travel.getName());

        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        format.setCurrency(Currency.getInstance("BRL"));

        holder.price.setText(format.format(travel.getPrice()));

        if (travel.getImages() != null && !TextUtils.isEmpty(travel.getImages().get(0))) {
            Picasso.with(context)
                    .load(travel.getImages().get(0))
                    .placeholder(R.drawable.placeholder)
                    .fit()
                    .centerInside()
                    .into(holder.image);
        } else {
            holder.image.setImageResource(R.drawable.placeholder);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TravelActivity.class);

                Gson gson = new Gson();
                String jsonTravel = gson.toJson(travel);
                intent.putExtra("Travel", jsonTravel);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return travels.size();
    }

    public void setTravels(List<Travel> travels) {
        this.travels = travels;
        notifyDataSetChanged();
    }

}
